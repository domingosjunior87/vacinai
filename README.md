# Vacinaí - Carteira Digital de Vacinação

O Vacinaí é um sistema que auxilia a população a estar sempre em dia com suas vacinas, porque, além de disponibilizar o histórico de vacinas tomadas, indica a próxima vacina a ser tomada e a unidade de saúde mais perto.

## Protótipo
O protótipo navegável do sistema encontra-se no PDF `prototipo_navegacao.pdf` que se encontra na raiz desse projeto. Basta efetuar o download do arquivo e navegar pelo mesmo.